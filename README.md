# linux-macspoofer

## Automatic macspoofer for linux / Debian

### Setup
1. Copy the macspoofer-script into your /etc/init.d/ directory
2. Install the net-tool package with, for example: `apt install net-tools`
3. Run the following commands:

`update-rc.d macspoofer defaults`
`update-rc.d macspoofer enable`

4. Edit the "ifaces"-variable in /etc/init.d/macspoofer to your needs
5. Reboot

You now should have an other MAC after rebooting.
